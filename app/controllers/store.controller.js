const db = require("../models");
const Store = db.stores;


// Retrieve all Faxes from the database.
exports.findAll = (req, res) => {
  // const itemNumber = req.query.itemNumber;
  // var condition = itemNumber ? { itemNumber: { $regex: new RegExp(itemNumber), $options: "i" } } : {};
  Store.find()
    .then(data => {
      res.send(data);
      console.log(data)
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving stores."
      });
    });
};

//https://bezkoder.com/node-express-mongodb-crud-rest-api/


// exports.findAll = (req, res) => {
//     const itemNumber = req.query.itemNumber;
//     var condition = itemNumber ? { itemNumber: { $regex: new RegExp(itemNumber), $options: "i" } } : {};
  
//     Tutorial.find(condition)
//       .then(data => {
//         res.send(data);
//       })
//       .catch(err => {
//         res.status(500).send({
//           message:
//             err.message || "Some error occurred while retrieving stores."
//         });
//       });
//   };