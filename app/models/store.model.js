module.exports = mongoose => {
  const Store = mongoose.model(
      "store",
      mongoose.Schema(
        {
          itemNumber: String,
          upc: String,
          itemName: String,
          sale: Number,
          size: String,
          image: String,
          price: String,
          quant: Number,
          amount: Number,
        }
      )
    );
    return Store;
  };

