import shop from "../../api/shop";

// initial state
// shape: [{ id, quantity }]
const state = () => ({
  items: [],
  checkoutStatus: null
});

// getters
const getters = {
  cartProducts: (state, getters, rootState) => {
    return state.items.map(({ itemNumber, quantity }) => {
      const product = rootState.products.all.find(product => product.itemNumber === itemNumber);
      return {
        sale: product.sale,
        price: product.price,
        quantity
      };
    });
  },
  stripeCart: (state, getters, rootState) => {
    return state.items.map(({ itemNumber, quantity }) => {
      const product = rootState.products.all.find(product => product.itemNumber === itemNumber);
      return {
        price: product.price,
        quantity
      };
    });
  },

  cartTotalPrice: (state, getters) => {
    return getters.cartProducts.reduce((total, product) => {
      return total + product.sale * product.quantity;
    }, 0);
  }
};

// actions
const actions = {
  checkout({ commit, state }, products) {
    const savedCartItems = [...state.items];
    commit("setCheckoutStatus", null);
    // empty cart
    commit("setCartItems", { items: [] });
    shop.buyProducts(
      products,
      () => commit("setCheckoutStatus", "successful"),
      () => {
        commit("setCheckoutStatus", "failed");
        // rollback to the cart saved before sending the request
        commit("setCartItems", { items: savedCartItems });
      }
    );
  },

  addProductToCart({ state, commit }, product) {
    commit("setCheckoutStatus", null);
    const cartItem = state.items.find(item => item.itemNumber === product.itemNumber);
    if (!cartItem) {
      commit("pushProductToCart", { itemNumber: product.itemNumber });
    } else {
      commit("incrementItemQuantity", cartItem);
    }
    // remove 1 item from stock
    //   commit('products/decrementProductInventory', { id: product.id }, { root: true })
  }
};

// mutations
const mutations = {
  pushProductToCart(state, { itemNumber }) {
    state.items.push({
      itemNumber,
      quantity: 1
    });
  },

  incrementItemQuantity(state, { itemNumber }) {
    const cartItem = state.items.find(item => item.itemNumber === itemNumber);
    cartItem.quantity++;
  },

  setCartItems(state, { items }) {
    state.items = items;
  },

  setCheckoutStatus(state, status) {
    state.checkoutStatus = status;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
