// import shop from "../../api/shop";
import axios from "axios";

// initial state
const state = () => ({
  all: []
});

// getters
const getters = {};

// actions
const actions = {
  async getAllProducts({ commit }) {
    const token = await this.$auth.getTokenSilently();

    // Use Axios to make a call to the API
    const { products } = await axios.get("/stores", {
      baseURL: "http://localhost:8090/api/",
      headers: {
        Authorization: `Bearer ${token}`, // send the access token through the 'Authorization' header
      },
    });
    this.stores = products;
    commit("setProducts", products);
  }
};

// mutations
const mutations = {
  setProducts(state, products) {
    state.all = products;
  }

  //   decrementProductInventory (state, { id }) {
  //     const product = state.all.find(product => product.id === id)
  //     product.inventory--
  //   }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
