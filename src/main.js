import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Argon from "./plugins/argon-kit";
import { Auth0Plugin } from "../src/auth";
import { domain, clientId, audience } from "../auth_config.json";
import * as VueStripeCheckout from "vue-stripe-checkout";
// import { currency } from "./currency";
// import vuetify from './plugins/vuetify';

Vue.use(VueStripeCheckout, "pk_test_xuuaEcmuwtheU8OlrsGCp8nn00UXYaikzu");
// Vue.filter("currency", currency);

Vue.use(Auth0Plugin, {
  domain,
  clientId,
  audience,
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});

Vue.config.productionTip = false;
Vue.use(Argon);

new Vue({
  router,
  store,
  // vuetify,
  render: h => h(App)
}).$mount("#app");
