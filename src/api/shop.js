/**
 * Mocking client-server processing
 */
import axios from "axios";


export default {
  async getProducts(cb) {
    const token = await this.$auth.getTokenSilently();
    const { data } = await axios.get("/stores", {
      baseURL: "http://localhost:8090/api/",
      headers: {
        Authorization: `Bearer ${token}`, // send the access token through the 'Authorization' header
      },
    });
    const _products = data;
    setTimeout(() => cb(_products), 100);

    console.log(this.stores);
  },
  // getProducts(cb) {
  //   setTimeout(() => cb(_products), 100);
  // },

  buyProducts(products, cb, errorCb) {
    setTimeout(() => {
      // simulate random checkout failure.
      Math.random() > 0.5 || navigator.webdriver ? cb() : errorCb();
    }, 100);
  }
};
