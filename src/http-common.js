import axios from "axios";

export default axios.create({
  baseURL: "http://167.99.190.100:8090/api",
  // baseURL: "https://portal.pharmfax.com/api",
  headers: {
    "Content-type": "application/json"
  }
});
