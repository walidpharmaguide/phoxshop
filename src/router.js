import Vue from "vue";
import Router from "vue-router";
import AppHeader from "./layout/AppHeader";
import AppFooter from "./layout/AppFooter";
import Presentation from "./views/Presentation.vue";
// import Components from "./views/Components.vue";
import Sections from "./views/Sections.vue";
import About from "./views/About.vue";
import BlogPosts from "./views/BlogPosts.vue";
import BlogPost from "./views/BlogPost.vue";
import ContactUs from "./views/ContactUs.vue";
import LandingPage from "./views/LandingPage.vue";
import Pricing from "./views/Pricing.vue";
import Ecommerce from "./views/Ecommerce.vue";
import ProfilePage from "./views/ProfilePage.vue";
import Error from "./views/Error.vue";
import Error500 from "./views/Error500.vue";
import Subscription from "./views/Subscription.vue";
import Login from "./views/Login.vue";
import Reset from "./views/Reset.vue";
import Invoice from "./views/Invoice.vue";
import ChatPage from "./views/ChatPage.vue";
import ProductPage from "./views/ProductPage.vue";
import Account from "./views/Account.vue";
import Checkout from "./views/Checkout.vue";
import { authGuard } from "../src/auth";
import ShoppingCart from "./views/ShoppingCart.vue";
import ProductList from "./views/ProductList.vue";
import Faxes from "./views/Faxes.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  // base: process.env.BASE_URL,
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "ecommerce",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Ecommerce,
        footer: AppFooter
      }
    },
    {
      path: "/presentation",
      name: "presentation",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Presentation,
        footer: AppFooter
      },
      props: {
        header: { navbarType: "primary" }
      }
    },
    {
      path: "/sections",
      name: "sections",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Sections,
        footer: AppFooter
      },
      props: {
        header: { navbarType: "default" }
      }
    },
    {
      path: "/about",
      name: "about",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: About,
        footer: AppFooter
      },
      props: {
        header: { navbarType: "primary" }
      }
    },
    {
      path: "/blog-posts",
      name: "blog-posts",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: BlogPosts,
        footer: AppFooter
      }
    },
    {
      path: "/blog-post",
      name: "blog-post",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: BlogPost,
        footer: AppFooter
      }
    },
    {
      path: "/shopping-cart",
      name: "shopping-cart",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: ShoppingCart,
        footer: AppFooter
      }
    },
    {
      path: "/product-list",
      name: "product-list",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: ProductList,
        footer: AppFooter
      }
    },
    {
      path: "/contact-us",
      name: "contact-us",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: ContactUs,
        footer: AppFooter
      },
      props: {
        header: { navbarType: "default" }
      }
    },
    {
      path: "/landing-page",
      name: "landing-page",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: LandingPage,
        footer: AppFooter
      }
    },
    {
      path: "/pricing",
      name: "pricing",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Pricing,
        footer: AppFooter
      },
      props: {
        header: { navbarType: "primary" }
      }
    },
    // {
    //   path: "/ecommerce",
    //   name: "ecommerce",
    //   beforeEnter: authGuard,
    //   components: {
    //     header: AppHeader,
    //     default: Ecommerce,
    //     footer: AppFooter
    //   }
    // },
    {
      path: "/profile-page",
      name: "profile-page",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: ProfilePage,
        footer: AppFooter
      }
    },
    {
      path: "/error",
      name: "error",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Error
      },
      props: {
        header: { navbarType: "default" }
      }
    },
    {
      path: "/500-error",
      name: "500-error",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Error500
      },
      props: {
        header: { navbarType: "primary" }
      }
    },
    {
      path: "/subscription",
      name: "subscription",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Subscription,
        footer: AppFooter
      }
    },
    {
      path: "/login",
      name: "login",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Login,
        footer: AppFooter
      }
    },
    {
      path: "/reset",
      name: "reset",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Reset,
        footer: AppFooter
      }
    },
    {
      path: "/invoice",
      name: "invoice",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Invoice,
        footer: AppFooter
      }
    },
    {
      path: "/chat-page",
      name: "chat-page",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: ChatPage,
        footer: AppFooter
      }
    },
    {
      path: "/product-page",
      name: "product-page",
      // beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: ProductPage,
        footer: AppFooter
      }
    },
    {
      path: "/account",
      name: "account",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Account,
        footer: AppFooter
      }
    },
    {
      path: "/checkout",
      name: "checkout",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Checkout,
        footer: AppFooter
      }
    },
    {
      path: "/faxes",
      name: "faxes",
      beforeEnter: authGuard,
      components: {
        header: AppHeader,
        default: Faxes,
        footer: AppFooter
      }
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
