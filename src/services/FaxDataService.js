import http from "../http-common";

class FaxDataService {
  getAll() {
    return http.get("/store");
  }
}

export default new FaxDataService();
