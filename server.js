const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

var fs = require('fs');
// var http = require('http');
// var https = require('https');

// var privateKey  = fs.readFileSync('sslcert/key.pem', 'utf8');
// var certificate = fs.readFileSync('sslcert/cert.pem', 'utf8');

// var credentials = {key: privateKey, cert: certificate};

// var httpServer = http.createServer(app);
// var httpsServer = https.createServer(credentials, app);

var jwt = require("express-jwt");
var jwks = require("jwks-rsa");
var app = express();

// const app = express();
var jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: "https://pharmaguide.auth0.com/.well-known/jwks.json"
  }),
  audience: "https://api.pharmaguide.ca/",
  issuer: "https://pharmaguide.auth0.com/",
  algorithms: ["RS256"]
});

app.get("/api/external", jwtCheck, (req, res) => {
  res.send({
    msg: "Your Access Token was successfully validated!"
  });
});
app.use(jwtCheck);

app.get("/authorized", function(req, res) {
  res.send("Secured Resource");
});
var corsOptions = {
  origin: "http://localhost:3000"
  // origin: "https://store.pharmaguide.ca"
};

const db = require("./app/models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to PharmaGuide application." });
});
require("./app/routes/store.routes")(app);
// set port, listen for requests
const PORT = process.env.PORT || 8090;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

// httpServer.listen(8080);
// httpsServer.listen(8090);